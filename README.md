几个常用的动画代码。

使用前需

* 引入QuartzCore.framework, 并在相关文件中加入 #import "QuartzCore/QuartzCore.h" 

* 定义 shakeFeedbackOverlay为UIImageView

* 设置

self.shakeFeedbackOverlay.alpha = 0.0; 

self.shakeFeedbackOverlay.layer.cornerRadius = 10.0; //设置圆角半径
 